const validator =  require('validator');
const mongoose = require('mongoose');
const studentsSchema = new mongoose.Schema({

    name:{
        type:String,
        required:true,
        lowercase:true,
        trim:true,
        // minlength:[3, "minlength length is 3 "],
        // mixlength:[5, "mixlength length is 5"]
    },
   
    phone :{
        type:Number,
        required:true,        
        // validate(value){
        //      if (value.toString().length<10 ||value.toString().length>10) {
        //      throw console.error("phone no. is incorrect");  
        //    }
        // } 

   },
   email:{
       type:String,
       required:true,       
    //    validate(value){
    //        if (!validator.isEmail(value)) {
    //            throw  Error("email  is invalid");    
    //        }
    //    }
   },
   message:{
    type:String,
    required:true,   

  },
  date:{
   type:Date,
   default:Date.now

}
})

const student=new mongoose.model("Student",studentsSchema);
module.exports =student;